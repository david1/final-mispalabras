from django.contrib import admin
from .models import Palabra,Comentario,Votos,Enlace
# Register your models here.

admin.site.register(Palabra)
admin.site.register(Comentario)
admin.site.register(Votos)
admin.site.register(Enlace)