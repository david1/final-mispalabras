from django.db import models

class Palabra(models.Model):
    nombre = models.CharField(max_length=50)
    nvotos = models.IntegerField()
    imagen_wiki = models.CharField(max_length=300)
    imagen_flickr = models.CharField(max_length=300)
    resumen = models.TextField()
    definicion = models.TextField()
    creador = models.CharField(max_length=50)
    fecha = models.DateTimeField()

class Comentario(models.Model):
    texto = models.TextField()
    palabra = models.CharField(max_length=50)
    usuario = models.CharField(max_length=20)
class Votos(models.Model):
    palabra = models.CharField(max_length=50)
    usuarios = models.TextField()
class Enlace(models.Model):
    palabra = models.ForeignKey('Palabra',on_delete=models.CASCADE)
    enlace = models.URLField()
    usuario = models.CharField(max_length=30)
    descripcion = models.TextField()
    imagen = models.CharField(max_length=300)