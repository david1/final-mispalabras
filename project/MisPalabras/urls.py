from django.urls import path, include
from . import views
from django.contrib.auth.views import LoginView

urlpatterns = [
    path('login', LoginView.as_view(), name='login'),
    path('logout', views.logout_view, name='logout'),
    path('signup', views.crear_cuenta, name='crear_cuenta'),

    path('user/<user>', views.user, name='user'),
    path('ayuda', views.ayuda, name ='ayuda'),
    path('contenidosXML', views.contenidos_XML),
    path('contenidosJSON', views.contenidos_JSON),
    path('<pal>', views.palabra, name='palabra'),
    path('', views.main),

]
