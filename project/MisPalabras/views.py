from django.shortcuts import render, redirect
from django.contrib.auth import logout, authenticate, login
from django.contrib.auth.forms import UserCreationForm
from random import randint
import urllib.request
import json
from django.http import HttpResponse
from pyrae import dle
from .models import Palabra, Comentario, Votos, Enlace
from datetime import datetime
from xml.etree.ElementTree import Element, tostring
import requests
import xmltodict
import operator
from bs4 import BeautifulSoup




def lista_5_palabras(pag):
    lista_final = []
    palabrasbase = Palabra.objects.all().values()
    lista = (sorted(palabrasbase, key=lambda x: x['fecha'], reverse=True))
    for obj in lista:
        elemento = (obj['nombre'], obj['imagen_wiki'], obj['creador'],obj['nvotos'],  obj['resumen'],  obj['fecha'])
        lista_final.append(elemento)
    return lista_final[5*(int(pag)-1):(5+5*(int(pag)-1))]
    #return lista_final

def main(request):
    if request.method == 'POST':
        palabra = request.POST.get('palabra')
        url = 'http://localhost:8000/' + palabra
        return redirect(url)
    else:
        data = {}
        npal = 0
        pag = request.GET.get('pag', 1)
        for obj in Palabra.objects.all():
            data[obj.nombre] = obj.nvotos
            npal += 1
        resto = npal % 5
        if resto == 0:
            npag = npal // 5
        else:
            npag = npal // 5 + 1
        data_sorted = sorted(data.items(), key=operator.itemgetter(1), reverse=True)[0:10]
        for obj in Palabra.objects.all():
            data[obj.nombre] = (obj.imagen_wiki, obj.creador,obj.nvotos,  obj.resumen,  obj.fecha)
        context ={
            'palabras': lista_5_palabras(pag),
            'num_paginas': list(range(1,npag +1)),
            'palabras_ordenadas': data_sorted,
            'npalabras': npal,
        }
        palabra_random = list(data.keys())[randint(0, len(context['palabras'])-1)]
        context['palabra'] = palabra_random
        return render(request,'main.html',context=context)

def logout_view(request):
    logout(request)
    return redirect("/")

def resumen_wiki(pal):
    url = 'https://es.wikipedia.org/w/api.php?action=query&format=xml&titles='+pal+'&prop=extracts&exintro&explaintext'
    try:
        file = requests.get(url, allow_redirects=True)
        dict_xml = xmltodict.parse(file.content)
        return dict_xml['api']['query']['pages']['page']['extract']['#text']
    except KeyError:
        return 'La palabra '+ pal + ' no existe en wikipedia'

def imagen_wiki(pal):
    url = 'https://es.wikipedia.org/w/api.php?action=query&titles='+pal+'&prop=pageimages&format=json&pithumbsize=200'
    try:
        with urllib.request.urlopen(url) as json_doc:
            json_str = json_doc.read()
            contenido = json.loads(json_str)
            a = list(contenido['query']['pages'].keys())
            return contenido['query']['pages'][a[0]]['thumbnail']['source']
    except KeyError:
        return 'https://transformacionagil.files.wordpress.com/2021/05/no_existe.jpg?w=880&h=312&crop=1'

def imagen_flickr(pal):
    flickrurl = 'https://www.flickr.com/services/feeds/photos_public.gne?tags='+pal
    try:
        file = requests.get(flickrurl, allow_redirects=True)
        dict_xml = xmltodict.parse(file.content)
        return dict_xml['feed']['entry'][0]['link'][1]['@href']
    except KeyError:
        return 'https://transformacionagil.files.wordpress.com/2021/05/no_existe.jpg?w=880&h=312&crop=1'
def deficion_rae(palabra):
    url = "https://dle.rae.es/" + palabra
    user_agent = "Mozilla/5.0 (X11) Gecko/20100101 Firefox/100.0"
    headers = {'User-Agent': user_agent}
    req = urllib.request.Request(url, headers=headers)
    with urllib.request.urlopen(req) as response:
        html = response.read().decode('utf-8')
    soup = BeautifulSoup(html, 'html.parser')
    definicion = soup.find("meta", {"property": "og:description"})
    # if not (definicion["content"].startswith("1.")):
    #     return "Texto no encontrado en RAE"
    if definicion["content"] =="Versión electrónica 23.5 del «Diccionario de la lengua española», obra lexicográfica académica por excelencia.":
        return "No hay definicion en la rae de " + palabra
    else:
        return definicion["content"]
def enlace_info(url):
    try:
        user_agent = "Mozilla/5.0 (X11) Gecko/20100101 Firefox/100.0"
        headers = {'User-Agent': user_agent}
        req = urllib.request.Request(url, headers=headers)
        with urllib.request.urlopen(req) as response:
            html = response.read().decode('utf-8')
        soup = BeautifulSoup(html, 'html.parser')
        descripcion = soup.find("meta", {"property": "og:description"})["content"]
        imagen = soup.find("meta", {"property": "og:image"})["content"]
        if descripcion is None:
            descripcion = soup.find("meta", {"property": "og:title"})["content"]
            if descripcion is None:
                descripcion = ""
        return descripcion, imagen
    except:
        return "No hay descripcion", "https://transformacionagil.files.wordpress.com/2021/05/no_existe.jpg?w=880&h=312&crop=1"

def palabra(request, pal):
    data = {}
    for obj in Palabra.objects.all():
        data[obj.nombre] = (obj.imagen_wiki, obj.creador, obj.nvotos, obj.resumen,  obj.fecha)
    if pal in data:
        esta_almacenada = True
        nvotos = data[pal][2]
        creador = data[pal][1]
    else:
        esta_almacenada = False
        creador = False
        nvotos = 0

    if request.method == 'GET':
        if nvotos == 0:
            objeto = Votos(palabra=pal)
        else:
            objeto = Votos.objects.get(palabra=pal)
        lista2 = objeto.usuarios.split(',')
        ha_votado = request.user.get_username() in lista2
        comentarios = {}
        enlaces = {}
        for obj in Comentario.objects.all():
            if obj.palabra == pal:
                comentarios[obj.usuario] = []
        for obj in Comentario.objects.all():
            if obj.palabra == pal:
                comentarios[obj.usuario].append(obj.texto)
        for obj in Enlace.objects.all():
            if obj.palabra.nombre == pal:
                enlaces[obj.usuario] = []
        for obj in Enlace.objects.all():
            if obj.palabra.nombre == pal:
                enlaces[obj.usuario].append([obj.enlace,obj.descripcion, obj.imagen])
        context = {
            'pal': pal,
            'resumen': resumen_wiki(pal),
            'link_imagen': imagen_wiki(pal),
            'esta_almacenada': esta_almacenada,
            'comentarios':comentarios,
            #'def': dle.search_by_word(word=pal),
            'def': deficion_rae(pal),
            'nvotos': nvotos,
            'creador': creador,
            'imagen_flickr': imagen_flickr(pal),
            'ha_votado': ha_votado,
            'enlaces': enlaces,
            }
        return render(request,'palabra.html',context=context)

    elif request.method == 'POST':
        if request.POST.get("almacenar") == 'almacenar':
            nueva_pal = Palabra(nombre=pal)
            nueva_pal.fecha = datetime.now()
            nueva_pal.nvotos = 0
            nueva_pal.imagen_flickr = imagen_flickr(pal)
            nueva_pal.creador = request.user.get_username()
            nueva_pal.resumen = resumen_wiki(pal)
            nueva_pal.imagen_wiki = imagen_wiki(pal)
            nueva_pal.definicion = dle.search_by_word(word=pal)
            nueva_pal.save()
        elif request.POST.get('comentario'):
            nuevo_comentario = Comentario(texto=request.POST.get('comentario'))
            nuevo_comentario.palabra = pal
            nuevo_comentario.usuario = request.user.get_username()
            nuevo_comentario.save()
        elif request.POST.get('voto'):
            palabra = Palabra.objects.get(nombre=pal)
            if palabra.nvotos == 0:
                objeto = Votos(palabra=pal)
            else:
                objeto = Votos.objects.get(palabra=pal)
            lista2 = objeto.usuarios.split(',')
            if not request.user.get_username() in lista2:
                palabra.nvotos += 1
                palabra.save()
                lista2.append(request.user.get_username())
                objeto.usuarios = (',').join(lista2)
                objeto.save()
        elif request.POST.get('meme'):
            return redirect("http://apimeme.com/meme?meme="+ request.POST.get('meme') + "&top=" + pal + "&bottom="+ request.POST.get('meme_abajo'))
        elif request.POST.get('enlace'):
            palabra = Palabra.objects.get(nombre=pal)
            nuevo_enlace = Enlace(palabra=palabra)
            nuevo_enlace.usuario = request.user.get_username()
            nuevo_enlace.enlace = request.POST.get('enlace')
            nuevo_enlace.descripcion, nuevo_enlace.imagen = enlace_info(request.POST.get('enlace'))
            nuevo_enlace.save()
        return redirect('/'+pal)

def user(request, user):
    p = Palabra.objects.all()
    lp = []
    for item in p:
        if item.creador == request.user.username:
            lp.append(item.nombre)
    c = Comentario.objects.all()
    lc = []
    for item in c:
        if item.usuario == request.user.username:
            lc.append(item.texto)
    lv = []
    context = {
        'user': user,
        'palabras': lp[::-1],
        'comentarios': lc[::-1],
    }
    return render(request, 'user.html', context=context)

def contenidos_XML(request):
    data = {}

    for obj in Palabra.objects.all():
        data[obj.nombre] = [obj.nombre, obj.imagen_wiki, obj.resumen,obj.nvotos,  obj.fecha]
    context = {
        'data':data
    }
    return render(request, 'contenido.xml',context=context,content_type='text/xml')

def contenidos_JSON(request):
    data = {}
    for obj in Palabra.objects.all():
        data[obj.nombre] = [obj.resumen, obj.imagen_wiki,obj.nvotos]

    context ={
         'palabras': data,
    }
    return render(request, 'contenido.json', context=context,content_type='text/json')

def ayuda(request):
    return render(request, 'ayuda.html')
def crear_cuenta(request):
    if request.method == 'POST':
        form = UserCreationForm(request.POST)
        if form.is_valid():
            form.save()
            username = form.cleaned_data.get('username')
            raw_password = form.cleaned_data.get('password1')
            user = authenticate(username=username, password=raw_password)
            login(request, user)
            return redirect('/')
    else:
        form = UserCreationForm()
        return render(request, 'signup.html', {'form': form})

def json_palabra(request, pal):
    pass


