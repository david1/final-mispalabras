# Entrega practica
## Datos
* Nombre: David
* Titulación: Ing de Sistemas de Teleco + ADE
* Despliegue (url): https://davidurjc.pythonanywhere.com/
* Video parte obligatoria (url):https://youtu.be/PKhmy0yPTYo
*
## Cuenta Admin Site
* usuario/contraseña admin/1234
## Cuentas usuarios
* usuario/contraseña david/david12345
* usuario/contraseña profe/profe12345
* usuario/contraseña holaquetal/baby12345
* ...
## Resumen parte obligatoria
Parte obligatoria terminada
## Lista partes opcionales
* Nombre parte: Añadido favicon
* Nombre parte: Añadido varias formas de acceder a la información de páginas web. Librerías específicas vs trocear HTML / JSON / XML
* Nombre parte: Posibilidad de guardar palabras que no existan
* Nombre parte: Foto de no existe cuando lo exista la foto en wikipedia o en flickr
